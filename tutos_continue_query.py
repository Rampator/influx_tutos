
from influxdb import InfluxDBClient
from influxdb import SeriesHelper

# InfluxDB connections settings
host = 'localhost'
port = 8086
user = 'root'
password = 'root'
dbname = 'example'

myclient = InfluxDBClient(host, port, user, password, dbname)


select_clause = 'SELECT mean("some_stat") INTO "events.stats" FROM "events.stats.us.east-1" GROUP BY time(1m)'

myclient.create_retention_policy("forever", "52w", "1", "example")

myclient.query("""
CREATE CONTINUOUS QUERY "rollup_1h" ON "example"
BEGIN
   SELECT mean(some_stat) INTO forever.example FROM "events.stats.us.east-1" GROUP BY time(1h)
END
""")
print(myclient.get_list_continuous_queries())
print(myclient.get_list_retention_policies())

